# -*- mode: tqfolder -*-

####################################################################################################################
### setup section ##################################################################################################
# this section contains the basic setup options ####################################################################
####################################################################################################################

+CreateModels {
  +HWWVBF {
    <applyStyles=false>

    # some basic settings
    +Parameters {
      <Title = "Run 2 VBF">
      <Lumi = 1.0>
      # The relative uncertainty on the luminosity (scale factor).
      # We model the luminosity uncertainty elswhere, so just set this to a very small, non-zero number
      <LumiRelErr = 1E-5>
      # The minimum bin content for each histogram bin
      <MinBinContent = 1E-12>
    }
    # settings for specific parameters
    +ParamSettings {
      # some detailed settings for individual parameters
      # set the default Lumi parameter to constant, as we have our own modelling for this
      +Lumi {
        <Const=true>
      }
    }

    # list all the samples with their respective paths
    +Samples {
    #  +vbf {
    #    <Type = S, Path = "sig/$(channelSys)/$(campaign)/$(sampleSys_vbf)">
    #  }
      +ggf {
        <Type = B, Path = "sig/$(channelSys)/$(campaign)/$(sampleSys_ggf)">
      }
      # WW samples, split in gg and qq production
      +diboson_qqWW {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_qqWW)">
      }
      +diboson_others {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_diboson_others)">
      }
      # Z+jets samples. Freely floating NFs, constraint from CR
      +Zjets_Sherpa {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_ztt)">
      }
      +Zjets_others {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_ztt_others)">
      }
      +wt {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_wt)">
      }
      +ttbar {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys_ttbar)">
      }
      $for(m,0,4){
        <forChannel.SR_bin$(m).Histogram = "CutVBFSR_jet1_pt/jet1_pt_bdt_vbf"> @*;
        <forChannel.TopWWCR_bin$(m).Histogram = "CutTopWWCR_jet1_pt/jet1_pt_bdt_TopWWAll"> @*;
      }
      <forChannel.SR_All.Histogram = "CutVBFSR_jet1_pt/jet1_pt_bdt_vbf"> @*;
      <forChannel.TopWWCR_All.Histogram = "CutTopWWCR_jet1_pt/jet1_pt_bdt_TopWWAll"> @*;
    }

#    +Edits {
#        +KillChannels {
#
#      }
#    }

    # list all the channels (=regions) with their respective cut and histogram names
#    +Channels{
#      # we remap to the range 0-1 to avoid confusion with unequal bin widths
#      +VBFSR{
#        <Histogram = "CutVBFSR/jet1_pt_bdt_vbf">
#        # New binning with two-fold DNN
#        #<mergeBinsManual=true, mergeBinsManual.borders={26, 60, 74, 84, 90, 94}>
#      }
#      +TopWWCR{
#        <Histogram = "CutTopWWCR/jet1_pt_bdt_TopWWAll">
#      }
#      +ZjetsCR {
#        <Histogram = "CutZjetsCR/MT">
#        # <Counter = "CutVBFZtautauControl_MTorMT2">
#      }
#      +ggF1CR {
#        <Histogram = "CutggFCR1/bdt_ggFCR1">
#      }
#      +ggF2CR {
#        <Histogram = "CutggFCR2/bdt_ggFCR2">
#      }
#      +ggF3CR {
#        <Histogram = "CutggFCR3/bdt_ggFCR3_CutDPhill">
#      }
#    }


    # list all the variations
    +Variations {
      # the sample folder from which we get all our inputs
      <SampleFolder = "sampleFolders/analyzed/samples-analyzed-SystAndNom-Differential-NewGGFBinning.root:samples">
      # import a few tags to the model, just for bookeeping
      <importTags={"luminosity>>LuminosityValue"}, lazy=false>
      # only load Nominal here. the experimental systematics variations are added later.
      +Nominal {
        #<SampleFolder = "sampleFolders/analyzed/samples-analyzed-Limited-VBF-V21-all.root:samples">
        <channelVar="",
        sampleVar_ztt="Zjets/Sherpa2p2p1/[Nom/tt+Lowmass/tt+Filter/tt]",
        sampleVar_ztt_others="Zjets/[Nom+Sherpa2p2p1/LowMass/ee+Sherpa2p2p1/LowMass/mm+Sherpa2p2p1/Nom/ee+Sherpa2p2p1/Nom/mm]",
        sampleVar_wt="Wt_PoPy",
        sampleVar_ttbar="ttbar_PoPy8/410472_s",
        sampleVar_qqWW="WW_Sherpa_qq_364254",
        sampleVar_diboson_others="diboson/[NonWW+WW/qq/VBS+WW/gg]",
        #sampleVar_vbf="vbf/PowPy8",
        sampleVar_ggf="ggf/PowPy8"
        >
      }
    }
  }
}

# we export the "raw" model, without any modifications (e.g. pruning) applied to it
+ExportModels.simple {
  +HWWVBF {
    <outputFile = "./workspaces/Background/jet1_pt/model-raw-test.root">
  }
}


####################################################################################################################
### Edit section ###################################################################################################
# this section contains various options and patches to be commented in and out for different studies ###############
####################################################################################################################
+CreateModels/HWWVBF/Channels{
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.CR.txt");
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.jet1_pt_SR.txt");
  <StatRelErrorThreshold=0.> @ ?;

}

+CreateModels/HWWVBF/Samples{
  <ActivateStatError = true, NormalizeByTheory = false> @ ?;
  <ActivateStatError = false, NormalizeByTheory = false> @ sig*;
  $replace("?:*", channel = "[em+me]", channelSys = "[em$(channelVar)+me$(channelVar)]");
  $replace("?:*", campaign = "[c16a+c16d+c16e]");
  $replace("?:*", sampleSys_ztt = "$(sampleVar_ztt)");
  $replace("?:*", sampleSys_ztt_others = "$(sampleVar_ztt_others)");
  $replace("?:*", sampleSys_wt = "$(sampleVar_wt)");
  $replace("?:*", sampleSys_ttbar = "$(sampleVar_ttbar)");
  $replace("?:*", sampleSys_qqWW = "$(sampleVar_qqWW)");
  $replace("?:*", sampleSys_diboson_others = "$(sampleVar_diboson_others)");
#  $replace("?:*", sampleSys_vbf = "$(sampleVar_vbf)");
  $replace("?:*", sampleSys_ggf = "$(sampleVar_ggf)");
}

+CreateModels/HWWVBF{
  $include("config/statistics/Differential-VBF-Sagar/systematics-theo-bkg.txt");

  <IsHistoSys=true> @ Systematics/theo_*;
  <Fallback = "Nominal"> @ Variations/?;
}

####################################################################################################################
### post-processing section ########################################################################################
# this section needs to go after the edit section, but usually no changes are required #############################
####################################################################################################################

#+CreateModels/?/Samples{
#  @? {
#    # apply lumi uncertainty only to those samples that are not normalized to data
#    # $escape(xyz) causes this block to be exited if the current folder is called "xyz" (or matches the expression in case wildcards are used)
#    $escape(ddFakes*);
#    $escape(top*);
#    $escape(ttbar*);
#    $escape(wt*);
#    $escape(Zjets*);
#    # $escape(*WW*);
#    $escape(Data*);
#
#    #we now add the Lumi nuisance parameter to all samples that we did not exclude
#    #preliminary "Moriond2019", check for updates at https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics
#    +OverallSys.ATLAS_LUMI {
#      <Val = 1., Low = 0.983, High = 1.017>
#    }
#    @ ? {
#      <scaleScheme = ".none">
#    }
#
#  }
#  @? {
#    $escape(Data);
#    # create normalization factors for each sample to allow easier extraction of postfit plots
#    # we set these constant, so they make no difference for the fit
#    +NormFactor.ATLAS_HWWlvlvGGF_sampleNorm_$(BASEFOLDERNAME) {
#      <Val = 1., Low = 0., High = 50., Const = true>
#    }
#  }
#  # activate MC stat error only on background samples
#  <ActivateStatError = true, NormalizeByTheory = false> @ ?;
#  <ActivateStatError = false, NormalizeByTheory = false> @ sig*;
#
#  # the following defines the standard layout for em+me fit
#  # the following defines the standard layout for em+me fit
#  $replace("?:*",channel = "[em+me]", channelSys = "[em$(Variation)+me$(Variation) $(addChannel1) $(addChannel2)]");
#
#  # the following defines the standard layout for the full run two fit
#  $replace("?:*",campaign = "[c16a+c16d+c16e]");
#  # use the following two to run on only a single campaign
#  # $replace("?:*",campaign = "c16a");
#
#  # use combined or split top / ttbar+Wt samples (delete those you don't want):
#  $delete("top!");
#  #$delete("ttbar!");
#  #$delete("wt!");
#
#}

# decide the name/label under which to produce this workspace
<saveConfig="./workspaces/Background/jet1_pt/buildModelConfig_test.txt">
#$replace("*:*",fitIdentifier="VBF-default-c16a");
