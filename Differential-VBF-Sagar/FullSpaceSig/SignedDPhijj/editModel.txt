+ImportModels {
  +HWWVBF {
    <inputFile = "./workspaces/SignedDPhijj/model-raw-test.root", objectName="HWWVBF">
  }
}

# export our model in a simple version before any edits (needed to visualize pruning/symmetrization in plots/tables)

#+ExportModels.simple {
#  +HWWVBF {
#    <outputFile = "./workspaces/model-simple-test.root">
#  }
#}


# do some post-processing on the model
# this is some advanced magic, you're a bit on your own here.
+EditModels.postprocessing {
  +HWWVBF {
    +ImportEdits.PostProcessing {
      <commands = {
        "@Channel.*/Sample.ggf* { $delete('OverallSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.ggf* { $delete('HistoSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.ggf* { $delete('OverallSys.theo_wt*!');  }",
        "@Channel.*/Sample.ggf* { $delete('HistoSys.theo_wt*!');  }",
        "@Channel.*/Sample.ggf* { $delete('OverallSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.ggf* { $delete('HistoSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.ggf* { $delete('OverallSys.theo_ww*!');  }",
        "@Channel.*/Sample.ggf* { $delete('HistoSys.theo_ww*!');  }",

        "@Channel.*/Sample.diboson_qqWW* { $delete('OverallSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('HistoSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('OverallSys.theo_wt*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('HistoSys.theo_wt*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('OverallSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('HistoSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('OverallSys.theo_ggF*!');  }",
        "@Channel.*/Sample.diboson_qqWW* { $delete('HistoSys.theo_ggF*!');  }",

        "@Channel.*/Sample.Zjets_Sherpa* { $delete('OverallSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('HistoSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('OverallSys.theo_wt*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('HistoSys.theo_wt*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('OverallSys.theo_ww*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('HistoSys.theo_ww*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('OverallSys.theo_ggF*!');  }",
        "@Channel.*/Sample.Zjets_Sherpa* { $delete('HistoSys.theo_ggF*!');  }",

        "@Channel.*/Sample.wt* { $delete('OverallSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.wt* { $delete('HistoSys.theo_ttbar*!');  }",
        "@Channel.*/Sample.wt* { $delete('OverallSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.wt* { $delete('HistoSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.wt* { $delete('OverallSys.theo_ww*!');  }",
        "@Channel.*/Sample.wt* { $delete('HistoSys.theo_ww*!');  }",
        "@Channel.*/Sample.wt* { $delete('OverallSys.theo_ggF*!');  }",
        "@Channel.*/Sample.wt* { $delete('HistoSys.theo_ggF*!');  }",

        "@Channel.*/Sample.ttbar* { $delete('OverallSys.theo_wt*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('HistoSys.theo_wt*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('OverallSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('HistoSys.theo_ztautau*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('OverallSys.theo_ww*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('HistoSys.theo_ww*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('OverallSys.theo_ggF*!');  }",
        "@Channel.*/Sample.ttbar* { $delete('HistoSys.theo_ggF*!');  }",

#        "@Channel.*/Sample.vbf* { $delete('OverallSys.*!');  }",
#        "@Channel.*/Sample.vbf* { $delete('HistoSys.*!');  }",

        "@Channel.*/Sample.diboson_others* { $delete('OverallSys.*!');  }",
        "@Channel.*/Sample.diboson_others* { $delete('HistoSys.*!');  }",

        "@Channel.*/Sample.Zjets_others* { $delete('OverallSys.*!');  }",
        "@Channel.*/Sample.Zjets_others* { $delete('HistoSys.*!');  }"
      }>
    }
  }
}

# export our model, both as a TQFolder and as a set of HistFactory XMLs
+ExportModels.final {
  +HWWVBF {
    <outputFile = "./workspaces/SignedDPhijj/model-final-test.root", writeXML="./workspaces/SignedDPhijj/xml">
  }
}

<saveConfig="workspaces/SignedDPhijj/editModelConfig.txt">
