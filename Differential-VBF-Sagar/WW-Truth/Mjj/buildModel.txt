# -*- mode: tqfolder -*-

####################################################################################################################
### setup section ##################################################################################################
# this section contains the basic setup options ####################################################################
####################################################################################################################

+CreateModels {
  +HWWVBF {
    <applyStyles=false>

    # some basic settings
    +Parameters {
      <Title = "Run 2 VBF">
      <Lumi = 1.0>
      # The relative uncertainty on the luminosity (scale factor).
      # We model the luminosity uncertainty elswhere, so just set this to a very small, non-zero number
      <LumiRelErr = 1E-5>
      # The minimum bin content for each histogram bin
      <MinBinContent = 1E-12>
    }
    # settings for specific parameters
    +ParamSettings {
      # some detailed settings for individual parameters
      # set the default Lumi parameter to constant, as we have our own modelling for this
      +Lumi {
        <Const=true>
      }
    }

    # list all the samples with their respective paths
    +Samples {
      +diboson_qqWW_truth {
        <Type = B, Path = "bkg/$(channelSys)/$(campaign)/$(sampleSys)">
      }
      $for(m,0,5){
        <forChannel.SR_bin$(m).Histogram = "CutVBFSR_Mjj/Mjj_bdt_vbf"> @*;
        <forChannel.TopWWCR_bin$(m).Histogram = "CutTopWWCR_Mjj/Mjj_bdt_TopWWAll"> @*;
      }
      <forChannel.SR_All.Histogram = "CutVBFSR_Mjj/Mjj_bdt_vbf"> @*;
      <forChannel.TopWWCR_All.Histogram = "CutTopWWCR_Mjj/Mjj_bdt_TopWWAll"> @*;
    }


    # list all the variations
    +Variations {
      # the sample folder from which we get all our inputs
      <SampleFolder = "sampleFolders/analyzed/samples-analyzed-theory-systs-ww-truth.root:samples">
      # import a few tags to the model, just for bookeeping
      <importTags={"luminosity>>LuminosityValue"}, lazy=false>
      # only load Nominal here. the experimental systematics variations are added later.
      +Nominal {
        <channelVar="",
        #sampleVar="diboson/[NonWW+WW/qq/VBS+WW/gg]",
        sampleVar="diboson/WW/qq/lvlv/Sherpa"
        >
      }
    }
  }
}

# we export the "raw" model, without any modifications (e.g. pruning) applied to it
+ExportModels.simple {
  +HWWVBF {
    <outputFile = "./workspaces/WW-Truth/Mjj/model-raw.root">
  }
}


####################################################################################################################
### Edit section ###################################################################################################
# this section contains various options and patches to be commented in and out for different studies ###############
####################################################################################################################
+CreateModels/HWWVBF/Channels{
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.CR.txt");
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.Mjj_SR.txt");
  <StatRelErrorThreshold=0.> @ ?;

}

+CreateModels/HWWVBF/Samples{
  <ActivateStatError = true, NormalizeByTheory = false> @ ?;
  <ActivateStatError = false, NormalizeByTheory = false> @ sig*;
  $replace("?:*", channel = "[em+me]", channelSys = "[em$(channelVar)+me$(channelVar)]");
  $replace("?:*", campaign = "mc16");
  $replace("?:*", sampleSys = "$(sampleVar)");
}

+CreateModels/HWWVBF{
  $include("config/statistics/Differential-VBF-Sagar/systematics-theo-ww-truth.txt");

  <IsHistoSys=true> @ Systematics/theo_*;
  <Fallback = "Nominal"> @ Variations/?;
}

<saveConfig="./workspaces/WW-Truth/Mjj/buildModelConfig.txt">
#$replace("*:*",fitIdentifier="VBF-default-c16a");
