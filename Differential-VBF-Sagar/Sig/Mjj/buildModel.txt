# -*- mode: tqfolder -*-

####################################################################################################################
### setup section ##################################################################################################
# this section contains the basic setup options ####################################################################
####################################################################################################################

+CreateModels {
  +HWWVBF {
    <applyStyles=false>

    # some basic settings
    +Parameters {
      <Title = "Run 2 VBF">
      <Lumi = 1.0>
      # The relative uncertainty on the luminosity (scale factor).
      # We model the luminosity uncertainty elswhere, so just set this to a very small, non-zero number
      <LumiRelErr = 1E-5>
      # The minimum bin content for each histogram bin
      <MinBinContent = 1E-12>
    }
    # settings for specific parameters
    +ParamSettings {
      # some detailed settings for individual parameters
      # set the default Lumi parameter to constant, as we have our own modelling for this
      +Lumi {
        <Const=true>
      }
    }

    # list all the samples with their respective paths
    +Samples {
      +vbf {
        <Type = S, Path = "sig/$(channelSys)/$(campaign)/$(sampleSys_vbf)">
      }
      $for(m,0,5){
        <forChannel.SR_bin$(m).Histogram = "CutVBFSR_Mjj/Mjj_bdt_vbf"> @*;
        <forChannel.TopWWCR_bin$(m).Histogram = "CutTopWWCR_Mjj/Mjj_bdt_TopWWAll"> @*;
      }
      <forChannel.SR_Migration.Histogram = "CutVBF_DPhill_truthP_recoP/Mjj_reco_truth"> @*;
      <forChannel.SR_RPTF.Histogram = "CutVBF_DPhill_truthF_recoP/Mjj"> @*;
      <forChannel.SR_RFTP.Histogram = "CutVBF_DPhill_truthP_recoF/Mjj_truth"> @*;
      <forChannel.FR.Histogram = "CutVBF_DPhill_truth/Mjj_truth"> @*;
    }


    # list all the variations
    +Variations {
      # the sample folder from which we get all our inputs
      <SampleFolder = "sampleFolders/analyzed/samples-analyzed-theory-systs-vbf-NewGGFBinning.root:samples">
      # import a few tags to the model, just for bookeeping
      <importTags={"luminosity>>LuminosityValue"}, lazy=false>
      # only load Nominal here. the experimental systematics variations are added later.
      +Nominal {
        <channelVar="",
        sampleVar_vbf="vbf/PowPy8"
        >
      }
    }
  }
}

# we export the "raw" model, without any modifications (e.g. pruning) applied to it
+ExportModels.simple {
  +HWWVBF {
    <outputFile = "./workspaces/Signal/Mjj/model-raw-test.root">
  }
}


####################################################################################################################
### Edit section ###################################################################################################
# this section contains various options and patches to be commented in and out for different studies ###############
####################################################################################################################
+CreateModels/HWWVBF/Channels{
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.CR.txt");
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.Mjj_SR.txt");
  $include("./config/statistics/Differential-VBF-Sagar/snippets/channels.Mjj_Sig.txt");
  <StatRelErrorThreshold=0.> @ ?;

}

+CreateModels/HWWVBF/Samples{
  <ActivateStatError = true, NormalizeByTheory = false> @ ?;
  <ActivateStatError = false, NormalizeByTheory = false> @ sig*;
  $replace("?:*", channel = "[em+me]", channelSys = "[em$(channelVar)+me$(channelVar)]");
  $replace("?:*", campaign = "[c16a+c16d+c16e]");
  $replace("?:*", sampleSys_vbf = "$(sampleVar_vbf)");
}

+CreateModels/HWWVBF{
  $include("config/statistics/Differential-VBF-Sagar/systematics-theo-sig.txt");

  <IsHistoSys=true> @ Systematics/theo_*;
  <Fallback = "Nominal"> @ Variations/?;
}


# decide the name/label under which to produce this workspace
<saveConfig="./workspaces/Signal/Mjj/buildModelConfig_test.txt">
