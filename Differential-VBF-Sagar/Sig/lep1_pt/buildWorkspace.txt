# -*- mode: tqfolder -*-

#instead import a prepared one (faster than rebuilding each time!)
+ImportModels {
  +HWWVBF {
    <inputFile = "./workspaces/Signal/lep1_pt/model-raw-test.root", objectName="HWWVBF">
  }
}

# actually create the workspace

+CreateWorkspaces {
  +HWWVBF{
    <logToFile= "./workspaces/Signal/lep1_pt/createWS.log">
    <histogramsFile = "./workspaces/Signal/lep1_pt/histograms.root">
  }
}


# write the workspace to disk

+ExportWorkspaces {
  +HWWVBF{
    <outputFile = "./workspaces/Signal/lep1_pt/workspace-preFit.root">
  }
}

<saveConfig="./workspaces/Signal/lep1_pt/buildWorkspaceConfig.txt">
#$replace("*:*",fitLabel="default");
