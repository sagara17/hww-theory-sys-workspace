+Variations {
 +vbf_nominal     { <channelVar="", sampleVar_vbf="vbf/PowPy8/345948_s">}
  # vbf PDF
  +vbf_pdf_0 { <channelVar="_PDFset90400", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_1 { <channelVar="_PDFset90401", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_2 { <channelVar="_PDFset90402", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_3 { <channelVar="_PDFset90403", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_4 { <channelVar="_PDFset90404", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_5 { <channelVar="_PDFset90405", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_6 { <channelVar="_PDFset90406", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_7 { <channelVar="_PDFset90407", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_8 { <channelVar="_PDFset90408", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_9 { <channelVar="_PDFset90409", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_10 { <channelVar="_PDFset90410", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_11 { <channelVar="_PDFset90411", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_12 { <channelVar="_PDFset90412", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_13 { <channelVar="_PDFset90413", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_14 { <channelVar="_PDFset90414", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_15 { <channelVar="_PDFset90415", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_16 { <channelVar="_PDFset90416", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_17 { <channelVar="_PDFset90417", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_18 { <channelVar="_PDFset90418", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_19 { <channelVar="_PDFset90419", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_20 { <channelVar="_PDFset90420", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_21 { <channelVar="_PDFset90421", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_22 { <channelVar="_PDFset90422", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_23 { <channelVar="_PDFset90423", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_24 { <channelVar="_PDFset90424", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_25 { <channelVar="_PDFset90425", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_26 { <channelVar="_PDFset90426", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_27 { <channelVar="_PDFset90427", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_28 { <channelVar="_PDFset90428", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_29 { <channelVar="_PDFset90429", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_pdf_30 { <channelVar="_PDFset90430", sampleVar_vbf="vbf/PowPy8/345948_s"> }

  # vbf_scale
  +vbf_SCALE_1    { <channelVar="_muR1.00muF0.50", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_SCALE_2    { <channelVar="_muR1.00muF2.00", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_SCALE_3    { <channelVar="_muR0.50muF1.00", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_SCALE_4    { <channelVar="_muR2.00muF1.00", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_SCALE_5    { <channelVar="_muR0.50muF0.50", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_SCALE_6    { <channelVar="_muR2.00muF2.00", sampleVar_vbf="vbf/PowPy8/345948_s"> }

  #vbf_shower
  +vbf_shower_PowH7     { <channelVar="", sampleVar_vbf="vbf/PowH7"> }

  #vbf_generator
  +vbf_generator_MGH7  { <channelVar="", sampleVar_vbf="vbf/MGH7"> }

  #vbf_alphas
  +vbf_alphas_1 { <channelVar="_PDFset90431", sampleVar_vbf="vbf/PowPy8/345948_s"> }
  +vbf_alphas_2 { <channelVar="_PDFset90432", sampleVar_vbf="vbf/PowPy8/345948_s"> }

}

+Systematics {
  +theo_vbf_pdf4lhc {
    <Up="vbf_pdf4lhc__1up",
    isDetectorSys = false, isSFSys = true, isFFSys = false,
    Attributes.0 = "theo", Attributes.1 = "NP"
    >
    +Compute/Up{
      <
      Mode="Hessian",
      Variations={
        "vbf_pdf_1",
        "vbf_pdf_2",
        "vbf_pdf_3",
        "vbf_pdf_4",
        "vbf_pdf_5",
        "vbf_pdf_6",
        "vbf_pdf_7",
        "vbf_pdf_8",
        "vbf_pdf_9",
        "vbf_pdf_10",
        "vbf_pdf_11",
        "vbf_pdf_12",
        "vbf_pdf_13",
        "vbf_pdf_14",
        "vbf_pdf_15",
        "vbf_pdf_16",
        "vbf_pdf_17",
        "vbf_pdf_18",
        "vbf_pdf_19",
        "vbf_pdf_20",
        "vbf_pdf_21",
        "vbf_pdf_22",
        "vbf_pdf_23",
        "vbf_pdf_24",
        "vbf_pdf_25",
        "vbf_pdf_26",
        "vbf_pdf_27",
        "vbf_pdf_28",
        "vbf_pdf_29",
        "vbf_pdf_30"
      },
      Baseline="vbf_pdf_0",
      Error="Nominal"
      >
    }
  }

  +theo_vbf_alphas {
    <Up="vbf_alphas__1up",
    isDetectorSys = false, isSFSys = true, isFFSys = false,
    Attributes.0 = "theo", Attributes.1 = "NP"
    >
    +Compute/Up {
      <
      Mode="Difference",
      Variations={"vbf_alphas_1","vbf_alphas_2"},
      Baseline="vbf_pdf_0",
      Error="Nominal"
      >
    }
  }

  +theo_vbf_scale
  {
    <
    Up="vbf_SCALE__1up",
    isDetectorSys = false, isSFSys = true, isFFSys = false,
    Attributes.0 = "theo", Attributes.1 = "NP"
    >
    +Compute/Up
    {
      <
      Mode="Envelope",
      Variations={"vbf_SCALE_1","vbf_SCALE_2","vbf_SCALE_3","vbf_SCALE_4","vbf_SCALE_5","vbf_SCALE_6"},
      Baseline="vbf_nominal"
      >
    }
  }

  +theo_vbf_shower {
    <Up="vbf_shower__1up",
    isDetectorSys = false, isSFSys = true, isFFSys = false,
    Attributes.0 = "theo", Attributes.1 = "NP"
    >
    +Compute/Up {
      <
      Mode="Single",
      Variations={"vbf_shower_PowH7"},
      Baseline="vbf_nominal"
      >
    }
  }

  +theo_vbf_generator {
    <Up="vbf_generator__1up",
    isDetectorSys = false, isSFSys = true, isFFSys = false,
    Attributes.0 = "theo", Attributes.1 = "NP"
    >
    +Compute/Up {
      <
      Mode="Single",
      Variations={"vbf_generator_MGH7"},
      Baseline="vbf_shower_PowH7"
      #Baseline="vbf_nominal"
      >
    }
  }
}
