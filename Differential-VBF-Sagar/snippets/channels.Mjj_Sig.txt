+SR_Migration{
      <Histogram = "CutVBF_DPhill_truthP_recoP/Mjj_reco_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=true>

}

+SR_RPTF{
      <Histogram = "CutVBF_DPhill_truthF_recoP/Mjj" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+SR_RFTP{
      <Histogram = "CutVBF_DPhill_truthP_recoF/Mjj_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+FR{
      <Histogram = "CutVBF_DPhill_truth/Mjj_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}
