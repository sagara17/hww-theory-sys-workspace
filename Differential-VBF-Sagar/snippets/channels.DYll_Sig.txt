+SR_Migration{
      <Histogram = "CutVBF_DPhill_truthP_recoP/DYll_reco_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=true>

}

+SR_RPTF{
      <Histogram = "CutVBF_DPhill_truthF_recoP/DYll" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+SR_RFTP{
      <Histogram = "CutVBF_DPhill_truthP_recoF/DYll_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+FR{
      <Histogram = "CutVBF_DPhill_truth/DYll_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}
