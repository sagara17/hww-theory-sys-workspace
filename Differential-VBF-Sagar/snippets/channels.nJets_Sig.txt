+SR_Migration{
      <Histogram = "CutVBF_DPhill_truthP_recoP/nJets_reco_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=true>

}

+SR_RPTF{
      <Histogram = "CutVBF_DPhill_truthF_recoP/nJets" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+SR_RFTP{
      <Histogram = "CutVBF_DPhill_truthP_recoF/nJets_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+FR{
      <Histogram = "CutVBF_DPhill_truth/nJets_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}
