@CreateModels/?{
    $include("config/statistics/Differential-VBF-Sagar/systematics-theo-bkg.txt");

    @Systematics {
        <IsHistoSys=true, IsOverallSys=true> @theo*;
    }
    @Variations {
        <channel = "[em$(channelVar)+me$(channelVar) $(addChannel1) $(addChannel2)]"> @ ?;
        <addChannel1 = "", addChannel2 = ""> @ ?;
    }
}
