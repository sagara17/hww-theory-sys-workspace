+SR_Migration{
      <Histogram = "CutVBF_DPhill_truthP_recoP/lep0_pt_reco_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=true>

}

+SR_RPTF{
      <Histogram = "CutVBF_DPhill_truthF_recoP/lep0_pt" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+SR_RFTP{
      <Histogram = "CutVBF_DPhill_truthP_recoF/lep0_pt_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}

+FR{
      <Histogram = "CutVBF_DPhill_truth/lep0_pt_truth" , includeUnderflow= false, includeOverflow = false, remap=false, ensureMinimumBinContent=false>

}
